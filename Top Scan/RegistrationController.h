//
//  RegistrationController.h
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 08.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *zipCode;

@end
