//
//  ViewController.h
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 08.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionPictures : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewCell *myCollectionItem;

@end

