//
//  ViewController.m
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 08.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import <Parse/Parse.h>
#import "ViewController.h"

#import "CollectionController.h"
#import "RegistrationController.h"
#import "RestoreController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize username;
@synthesize password;

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_wallpaper.png"]];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [self notificateCustomerWithAlert];
    [super didReceiveMemoryWarning];
}

#pragma mark - Authentificate Controller

- (IBAction)authWithCredentials:(id)sender {
    [PFUser logInWithUsernameInBackground:self.username.text password:self.password.text
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            CollectionController *collectionView = [[CollectionController alloc] initWithNibName:@"CollectionController" bundle:nil];
                                            [self presentViewController:collectionView animated:YES completion:nil];
                                        } else {
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Top Scan" message:@"Sorry. You enter incorrect account details or your account is not valid. Please check credentials and try again!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
                                            [alertView show];
                                        }
                                    }];
}

#pragma mark - Registration Controller

- (IBAction)registerController:(id)sender {
    RegistrationController *registrationView = [[RegistrationController alloc] initWithNibName:@"RegistrationController" bundle:nil];
    [self presentViewController:registrationView animated:YES completion:nil];
}

#pragma mark - Recovery Credentials Controller

- (IBAction)restoreCredentials:(id)sender {
    RestoreController *recoveryView = [[RestoreController alloc] initWithNibName:@"RestoreController" bundle:nil];
    [self presentViewController:recoveryView animated:YES completion:nil];
}

#pragma mark - Memory Warning Alert

- (void)notificateCustomerWithAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Top Scan" message:@"Dear user. Memory on this device is excised. Please restart your'e device. Thanks!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

@end
