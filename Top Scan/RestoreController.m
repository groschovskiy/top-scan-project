//
//  RestoreController.m
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 09.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import "RestoreController.h"
#import <Parse/Parse.h>

@interface RestoreController ()

@end

@implementation RestoreController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)restorePassword:(id)sender {
    [PFUser requestPasswordResetForEmailInBackground:self.emailAddress.text];
}

@end
