//
//  ClothingDetails.h
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 09.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClothingDetails : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *productBrand;
@property (weak, nonatomic) IBOutlet UILabel *productModel;
@property (weak, nonatomic) IBOutlet UILabel *productExtra;
@property (weak, nonatomic) IBOutlet UILabel *productDescription;
@property (weak, nonatomic) IBOutlet UILabel *productWashing;

@end
