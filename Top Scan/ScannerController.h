//
//  ScannerController.h
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 08.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScannerController : UIViewController

@property (strong) IBOutlet UICollectionView *clothingImages;
@property (strong) IBOutlet UICollectionViewCell *clothingCell;

@end
