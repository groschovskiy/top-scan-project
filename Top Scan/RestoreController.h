//
//  RestoreController.h
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 09.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestoreController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailAddress;

@end
