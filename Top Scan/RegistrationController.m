//
//  RegistrationController.m
//  Top Scan
//
//  Created by Groschovskiy Dmitriy on 08.05.15.
//  Copyright (c) 2015 Google Launchpad. All rights reserved.
//

#import "RegistrationController.h"
#import "CollectionController.h"
#import <Parse/Parse.h>

@interface RegistrationController ()

@end

@implementation RegistrationController

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_wallpaper.png"]];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerCustomer:(id)sender {
    PFUser *user = [PFUser user];
    user.username = self.username.text;
    user.password = self.password.text;
    user.email = self.username.text;
    
    user[@"firstName"] = self.firstName.text;
    user[@"lastName"] = self.lastName.text;
    user[@"zipCode"] = self.zipCode.text;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {   // Hooray! Let them use the app now.
        } else {   NSString *errorString = [error userInfo][@"error"];   // Show the errorString somewhere and let the user try again.
        }
    }];
}

@end
